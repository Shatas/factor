package com.company;

import java.util.ArrayList;

public class Number {

    private int value;
    private boolean isFactored;
    private ArrayList<Integer> factors;

    public Number(int number) {
        this.value = number;
        this.isFactored = false;
        this.factors = new ArrayList<>();
        this.addFactor(1);
    }

    public int getValue() {
        return this.value;
    }

    private void setIsFactored(boolean factored) {
        isFactored = factored;
    }

    private boolean getIsFactored() {
        return this.isFactored;
    }

    private void addFactor(int number) {
        this.factors.add(number);
    }

    private int getLastFactor() {
        return this.factors.get(this.factors.size() - 1);
    }

    public ArrayList<Integer> getFactors() {
        return this.factors;
    }

    public void factor(int number) {

        if (this.getIsFactored()) {
            System.out.println("Done!");
        }
        else {
            for (int i = 2; i <= number; i++) {
                if (number % i == 0) {
                    this.addFactor(i);
                    break;
                }
            }
            if (number == this.getLastFactor()) {
                this.setIsFactored(true);
                factor(number / this.getLastFactor());
            } else {
                factor(number / this.getLastFactor());
            }
        }
    }
}
